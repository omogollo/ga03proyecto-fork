package es.unex.giiis.asee.aseeproject.database;

import android.provider.BaseColumns;

public class DBContract {
    private DBContract(){};

    public static class BookItem implements BaseColumns{
        public static final String TABLE_NAME = "book";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_ISBN = "isbn";
        public static final String COLUMN_NAME_AUTHOR = "author";
        public static final String COLUMN_NAME_PUBLISHDATE = "publishdate";
        public static final String COLUMN_NAME_SYNOPSIS = "synopsis";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_IMAGE = "image";
        public static final String COLUMN_NAME_NUMPAGES = "numpages";
    }


    public static class BookFavourite implements BaseColumns{
        public static final String TABLE_NAME = "favouriteBooks";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_ISBN = "isbn";
        public static final String COLUMN_NAME_AUTHOR = "author";
        public static final String COLUMN_NAME_PUBLISHDATE = "publishdate";
        public static final String COLUMN_NAME_SYNOPSIS = "synopsis";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_IMAGE = "image";
        public static final String COLUMN_NAME_NUMPAGES = "numpages";
    }
}
