
package es.unex.giiis.asee.aseeproject.Models;

import com.google.gson.annotations.Expose;

public class Pdf {

    @Expose
    private Boolean isAvailable;

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

}
