package es.unex.giiis.asee.aseeproject.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;

import es.unex.giiis.asee.aseeproject.Fragments.BookFragment;
import es.unex.giiis.asee.aseeproject.Fragments.CategoryFragment;
import es.unex.giiis.asee.aseeproject.R;
import es.unex.giiis.asee.aseeproject.Models.BookItem;

public class MainActivity extends AppCompatActivity
        implements BookFragment.OnListFragmentInteractionListener,
        CategoryFragment.OnListFragmentInteractionListener {

    static private final String TAG = "MyBooks";
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private int currentFragment = 0;


    Toolbar myToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myToolbar = findViewById(R.id.my_toolbar);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        String barColor = PreferenceManager.getDefaultSharedPreferences(this).getString("color_tema", String.valueOf(getResources().getColor(R.color.green)));
        myToolbar.setBackgroundColor(Color.parseColor(barColor));
        setSupportActionBar(myToolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navView = findViewById(R.id.navview);
        navView.getHeaderView(0).setBackgroundColor(Color.parseColor(barColor));
        //navView.setItemBackground(new ColorDrawable(Color.parseColor(barColor)));

        toggle = new ActionBarDrawerToggle
                (
                        this,
                        drawerLayout,
                        R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close
                )
        {
        };

        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        boolean fragmentTransaction = false;
                        Fragment fragment = null;

                        switch (menuItem.getItemId()){
                            case R.id.menu_book_list:
                                currentFragment = 1;
                                fragment = new BookFragment();
                                fragmentTransaction = true;
                                break;
                            case R.id.menu_book_categories:
                                Log.i("NavigationDrawer: ", "Pulsado opcion categorias");
                                currentFragment = 2;
                                fragment = new CategoryFragment();
                                fragmentTransaction = true;
                                break;
                            case R.id.menu_book_favourites:
                                Log.i("NavigationDrawer: ", "Pulsado opcion favoritos");

                                currentFragment = 3;
                                fragment = new BookFragment();
                                fragmentTransaction = true;

                                break;
                        }
                        if(fragmentTransaction){
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container_main, fragment)
                                    .addToBackStack(null)
                                    .commit();
                            menuItem.setChecked(true);
                            getSupportActionBar().setTitle(menuItem.getTitle());
                        }
                        drawerLayout.closeDrawers();
                        return true;
                    }
                }
        );

        BookFragment mainFragment = new BookFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container_main, mainFragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item))
            return true;

        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                return true;
            case R.id.action_refresh:
                openRefresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openRefresh() {
        CharSequence text = "Refreshing...";
        int duration = Snackbar.LENGTH_SHORT;

        Snackbar.make(myToolbar, text, duration).setAction("Action", null).show();

        finish();

        Intent refresh = new Intent(this, MainActivity.class);
        overridePendingTransition(0, 0);
        startActivity(refresh);
        overridePendingTransition(0, 0);

    }


    @Override
    public void onBackPressed() {
        currentFragment=0;
        super.onBackPressed();
    }

    @Override
    public void onListFragmentInteraction(BookItem item) {
        if(myToolbar.getVisibility() == View.VISIBLE) {
            myToolbar.setVisibility(View.INVISIBLE);
        }
        else
            myToolbar.setVisibility(View.VISIBLE);
    }

    public int getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment (int pcurrentFragment) {
        currentFragment = pcurrentFragment;
    }

    @Override
    public void onListFragmentInteraction(String item) {
        if(myToolbar.getVisibility() == View.VISIBLE) {
            myToolbar.setVisibility(View.INVISIBLE);
        }
        else
            myToolbar.setVisibility(View.VISIBLE);

    }
}

