
package es.unex.giiis.asee.aseeproject.Models;

import com.google.gson.annotations.Expose;

public class SaleInfo {

    @Expose
    private String country;
    @Expose
    private String saleability;
    @Expose
    private Boolean isEbook;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSaleability() {
        return saleability;
    }

    public void setSaleability(String saleability) {
        this.saleability = saleability;
    }

    public Boolean getIsEbook() {
        return isEbook;
    }

    public void setIsEbook(Boolean isEbook) {
        this.isEbook = isEbook;
    }

}
