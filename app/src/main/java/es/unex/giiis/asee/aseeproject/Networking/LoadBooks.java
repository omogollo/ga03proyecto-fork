package es.unex.giiis.asee.aseeproject.Networking;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.unex.giiis.asee.aseeproject.AppExecutors;
import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.Models.Bookshelf;
import es.unex.giiis.asee.aseeproject.Models.Item;
import es.unex.giiis.asee.aseeproject.RoomDB.BookItemDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadBooks{

    private static RetrofitManager retrofit;
    LiveData<List<BookItem>> data;
    private static List<BookItem> bookitems;


    public LiveData<List<BookItem>> loadBooks (Context context, BookItemDao mbookItemDao, AppExecutors mExecutors, MutableLiveData<List<BookItem>> networkData){
        retrofit = new RetrofitManager(context);

        // Create a call instance for looking up repos.
        Call<Bookshelf> call = retrofit.getApiService().listRepos("109713644623575462613", 0, 40,"AIzaSyCTjTcgoHKX_DIUpFcNMUzclKzaWpG5CPM");

        Log.i("Tag", "Estoy antes de enque()");
        // Fetch and print a list of the contributors to the library.
        // Async call
        call.enqueue(new Callback<Bookshelf>() {
            @Override
            public void onResponse(Call<Bookshelf> call, Response<Bookshelf> response) {
                Log.i("Tag", "Estoy en onResponse()");
                if (response.isSuccessful()) {
                    // tasks available
                    Bookshelf items = response.body(); //para parsear creo la lista de objetos vols

                    //Guardarlos en la bd
                    data = saveAllBooks(items, mbookItemDao, mExecutors, networkData);


                    Log.i("Retrofit101", "Voy a procesar los libros");
                    for (Item item : items.getItems()) {
                        Log.i("Retrofit101","name: "+item.getVolumeInfo().getTitle());
                    }
                } else {
                    // error response, no access to resource?
                    Log.d("Retrofit101", "RESPONSE NO OK");
                }
            }

            @Override
            public void onFailure(Call<Bookshelf> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
            }
        });

        return data;
    }

    public LiveData<List<BookItem>> saveAllBooks(Bookshelf books, BookItemDao mbookItemDao, AppExecutors mExecutors, MutableLiveData<List<BookItem>> networkData){
        // final BookItemCRUD bookItemCRUD = BookItemCRUD.getInstance(context);

        long idLibro = 0;
        bookitems = new ArrayList<>();

        //Conversion de Item (Api) a BookItem (Clase modelo para un libro)
        for(Item i: books.getItems()){
            bookitems.add(itemToBookItem(i, idLibro));
            idLibro ++;
        }

        networkData.postValue(bookitems);

        networkData.observeForever(bookitems -> {
            mExecutors.diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    mbookItemDao.deleteAll();
                    for (BookItem b : bookitems){
                        mbookItemDao.insert(b);
                    }
                }
            });
        });

        return networkData;
    }

    /**
     * Convierte la informacion de un libro obtenida de la API, a una instancia de BookItem
     * @param item
     * @return
     */
    public BookItem itemToBookItem (Item item, long idLibro){
        BookItem bookItem;

        SimpleDateFormat FORMAT = new SimpleDateFormat(
                "yyyy-mm-dd", Locale.US);

        String title = item.getVolumeInfo().getTitle();
        String isbn = item.getVolumeInfo().getIndustryIdentifiers().get(0).getIdentifier();
        String author = item.getVolumeInfo().getAuthors().get(0);
        String publishDate = item.getVolumeInfo().getPublishedDate();
        String synopsis = item.getVolumeInfo().getDescription();
        String category = item.getVolumeInfo().getCategories().get(0);
        String image = item.getVolumeInfo().getImageLinks().getSmallThumbnail();
        String numpages = item.getVolumeInfo().getPageCount().toString();

        bookItem = new BookItem(idLibro,title,isbn,author,publishDate,synopsis,category,image,numpages);

        return bookItem;
    }
}
