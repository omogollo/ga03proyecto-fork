
package es.unex.giiis.asee.aseeproject.Models;

import com.google.gson.annotations.Expose;

public class ReadingModes {

    @Expose
    private Boolean text;
    @Expose
    private Boolean image;

    public Boolean getText() {
        return text;
    }

    public void setText(Boolean text) {
        this.text = text;
    }

    public Boolean getImage() {
        return image;
    }

    public void setImage(Boolean image) {
        this.image = image;
    }

}
