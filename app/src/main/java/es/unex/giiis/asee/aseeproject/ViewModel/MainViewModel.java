package es.unex.giiis.asee.aseeproject.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.Repository.BookFavouriteRepository;
import es.unex.giiis.asee.aseeproject.Repository.BookRepository;

public class MainViewModel extends ViewModel {

    private final BookRepository mBookRepository;
    private final BookFavouriteRepository mBookFavouriteRepository;
    private  LiveData<List<BookItem>> mBooks;
    private  LiveData<List<BookItem>> mBooksByCategory;
    private  LiveData<List<BookItem>> mFavouriteBooks;


    public MainViewModel(BookRepository bookRepository, BookFavouriteRepository bookFavouriteRepository) {
        this.mBookRepository = bookRepository;
        this.mBookFavouriteRepository = bookFavouriteRepository;
        this.mBooks = new MutableLiveData();
        this.mBooksByCategory = new MutableLiveData();
        this.mFavouriteBooks = new MutableLiveData();
    }


    public LiveData<List<BookItem>> getAll() {
        mBooks = mBookRepository.getAll();
        return mBooks;
    }

    public LiveData<List<BookItem>> getByCategory(String categoryName) {
        mBooksByCategory = mBookRepository.getByCategory(categoryName);
        return mBooksByCategory;
    }

    public LiveData<List<BookItem>> getAllFavourites() {
        mFavouriteBooks = mBookFavouriteRepository.getAll();
        return mFavouriteBooks;
    }
}
