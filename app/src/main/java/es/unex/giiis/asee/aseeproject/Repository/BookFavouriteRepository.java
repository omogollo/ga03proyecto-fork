package es.unex.giiis.asee.aseeproject.Repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.Executor;

import es.unex.giiis.asee.aseeproject.AppExecutors;
import es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem;
import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.RoomDB.BookDatabase;
import es.unex.giiis.asee.aseeproject.RoomDB.BookFavouriteItemDao;


public class BookFavouriteRepository {

    private Context context;
    private static BookFavouriteRepository instance;
    private final AppExecutors mExecutors;
    private final BookFavouriteItemDao mbookFavouriteItemDao;
    private static List<BookFavouriteItem> bookfavouriteitems;
    private MutableLiveData<BookFavouriteItem> data;


    public BookFavouriteRepository (Context context){
        this.context = context;
        mbookFavouriteItemDao = BookDatabase.getDatabase(context).bookFavouriteItemDao();
        mExecutors = AppExecutors.getInstance();
        data = new MutableLiveData<>();
    }

    public static BookFavouriteRepository getInstance(Context context) {
        if (instance == null) {
            return new BookFavouriteRepository(context);
        }
        else
            return instance;
    }

    public void insert (BookFavouriteItem bookFavouriteItem) {
        data.setValue(bookFavouriteItem);

        data.observeForever(bookfavourite -> {
            mExecutors.diskIO().execute(new Runnable() {
                @Override
                public void run() {

                    mbookFavouriteItemDao.insert(bookfavourite);

                }
            });
        });
    }

    public LiveData<List<BookItem>> getAll () {

        LiveData<List<BookItem>> data = mbookFavouriteItemDao.getAll();

        Executor executor= new Executor() {
            @Override
            public void execute(@NonNull Runnable command) {
                new Thread(command).start();
            }
        };

        return data;

    }

    public LiveData<List<BookItem>> getByIsbn (String isbn) {

        LiveData<List<BookItem>> data = mbookFavouriteItemDao.getByIsbn(isbn);


        Executor executor= new Executor() {
            @Override
            public void execute(@NonNull Runnable command) {
                new Thread(command).start();
            }
        };

        return data;
    }

    public void deleteByIsbn (String isbn) {
        mbookFavouriteItemDao.deleteByIsbn(isbn);

        Executor executor= new Executor() {
            @Override
            public void execute(@NonNull Runnable command) {
                new Thread(command).start();
            }
        };

    }
}
