package es.unex.giiis.asee.aseeproject.Activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;

import es.unex.giiis.asee.aseeproject.Fragments.BookDetailsFragment;
import es.unex.giiis.asee.aseeproject.R;
import es.unex.giiis.asee.aseeproject.Models.BookItem;

public class BookDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        ActionBar ab = getSupportActionBar();

        BookItem bookItem = (BookItem) getIntent().getSerializableExtra("bookItem");

        BookDetailsFragment bookDetailsFragment = new BookDetailsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        Log.i("GET(0)", bookItem.getIsbn());
        bundle.putSerializable("bookItem", bookItem);
        bookDetailsFragment.setArguments(bundle);

        fragmentTransaction.add(R.id.fragment_container_details, bookDetailsFragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        ab.setDisplayHomeAsUpEnabled(true);

        String barColor = PreferenceManager.getDefaultSharedPreferences(this).getString("color_tema", String.valueOf(getResources().getColor(R.color.green)));
        ab.setBackgroundDrawable(new ColorDrawable( (Color.parseColor(barColor)) ));

        ab.setTitle(R.string.app_name);
    }
}
