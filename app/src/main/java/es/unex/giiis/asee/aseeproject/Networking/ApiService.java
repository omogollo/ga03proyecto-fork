package es.unex.giiis.asee.aseeproject.Networking;

import es.unex.giiis.asee.aseeproject.Models.Bookshelf;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("users/{user}/bookshelves/{coleccion}/volumes")
    Call<Bookshelf> listRepos(@Path("user") String user, @Path("coleccion") Integer coleccion, @Query("maxResults") Integer maxResults, @Query("key") String key);
}
