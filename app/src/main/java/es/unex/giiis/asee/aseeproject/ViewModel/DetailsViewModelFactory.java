package es.unex.giiis.asee.aseeproject.ViewModel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.aseeproject.Repository.BookFavouriteRepository;
import es.unex.giiis.asee.aseeproject.Repository.BookRepository;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link Context}
 */
public class DetailsViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private static DetailsViewModelFactory instance;
    private final BookFavouriteRepository mBookFavouriteRepository;

    public DetailsViewModelFactory(Context context) {
        this.mBookFavouriteRepository = BookFavouriteRepository.getInstance(context);
    }

    public static DetailsViewModelFactory getInstance(Context context) {
        if (instance == null) {
            return new DetailsViewModelFactory(context);
        }
        else
            return instance;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetailsViewModel(mBookFavouriteRepository);
    }


}
