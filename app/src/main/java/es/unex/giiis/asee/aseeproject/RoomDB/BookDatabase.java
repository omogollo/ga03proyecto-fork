package es.unex.giiis.asee.aseeproject.RoomDB;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem;
import es.unex.giiis.asee.aseeproject.Models.BookItem;

@Database(entities = {BookItem.class, BookFavouriteItem.class}, version = 2, exportSchema = false)
public abstract class BookDatabase extends RoomDatabase {

    private static BookDatabase instance;

    public static BookDatabase getDatabase(Context context){
        if (instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), BookDatabase.class, "book.db").allowMainThreadQueries().fallbackToDestructiveMigration().build();
        return instance;
    }


    public abstract BookItemDao bookItemDao();
    public abstract BookFavouriteItemDao bookFavouriteItemDao();
}
