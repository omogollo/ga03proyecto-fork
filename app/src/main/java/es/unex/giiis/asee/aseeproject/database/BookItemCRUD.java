package es.unex.giiis.asee.aseeproject.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.aseeproject.Models.BookItem;

public final class BookItemCRUD {

    private BookManagerDbHelper mDbHelper;
    private static BookItemCRUD mInstance;

    private BookItemCRUD(Context context) {
        mDbHelper = new BookManagerDbHelper(context);
    }

    public static BookItemCRUD getInstance(Context context) {
        Log.i("SINGLETON: ", "Estoy aqui");
        if (mInstance == null)
            mInstance = new BookItemCRUD(context);

        return mInstance;
    }

    public List<BookItem> getAll() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                DBContract.BookItem._ID,
                DBContract.BookItem.COLUMN_NAME_TITLE,
                DBContract.BookItem.COLUMN_NAME_ISBN,
                DBContract.BookItem.COLUMN_NAME_AUTHOR,
                DBContract.BookItem.COLUMN_NAME_PUBLISHDATE,
                DBContract.BookItem.COLUMN_NAME_SYNOPSIS,
                DBContract.BookItem.COLUMN_NAME_CATEGORY,
                DBContract.BookItem.COLUMN_NAME_IMAGE,
                DBContract.BookItem.COLUMN_NAME_NUMPAGES,
        };

        String selection = null;
        String[] selectionArgs = null;

        String sortOrder = null;

        //es una SELECT
        //devuelve los resultados de forma genÃ©rica (abstracciÃ³n)
        Cursor cursor = db.query(
                DBContract.BookItem.TABLE_NAME,           // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );


        ArrayList<BookItem> items = new ArrayList<>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                items.add(getBookItemFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return items;
    }

    public List<BookItem> getByIsbn(String isbn) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                DBContract.BookItem._ID,
                DBContract.BookItem.COLUMN_NAME_TITLE,
                DBContract.BookItem.COLUMN_NAME_ISBN,
                DBContract.BookItem.COLUMN_NAME_AUTHOR,
                DBContract.BookItem.COLUMN_NAME_PUBLISHDATE,
                DBContract.BookItem.COLUMN_NAME_SYNOPSIS,
                DBContract.BookItem.COLUMN_NAME_CATEGORY,
                DBContract.BookItem.COLUMN_NAME_IMAGE,
                DBContract.BookItem.COLUMN_NAME_NUMPAGES,
        };

        String selection = "isbn=?";
        String[] selectionArgs = {isbn};

        String sortOrder = null;

        //es una SELECT
        //devuelve los resultados de forma genÃ©rica (abstracciÃ³n)
        Cursor cursor = db.query(
                DBContract.BookItem.TABLE_NAME,           // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );


        ArrayList<BookItem> items = new ArrayList<>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                items.add(getBookItemFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return items;
    }

    public List<BookItem> getByCategory(String categoryName) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                DBContract.BookItem._ID,
                DBContract.BookItem.COLUMN_NAME_TITLE,
                DBContract.BookItem.COLUMN_NAME_ISBN,
                DBContract.BookItem.COLUMN_NAME_AUTHOR,
                DBContract.BookItem.COLUMN_NAME_PUBLISHDATE,
                DBContract.BookItem.COLUMN_NAME_SYNOPSIS,
                DBContract.BookItem.COLUMN_NAME_CATEGORY,
                DBContract.BookItem.COLUMN_NAME_IMAGE,
                DBContract.BookItem.COLUMN_NAME_NUMPAGES,
        };

        String selection = "category=?";
        String[] selectionArgs = {categoryName};

        String sortOrder = null;

        //es una SELECT
        //devuelve los resultados de forma genÃ©rica (abstracciÃ³n)
        Cursor cursor = db.query(
                DBContract.BookItem.TABLE_NAME,           // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );


        ArrayList<BookItem> items = new ArrayList<>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                items.add(getBookItemFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return items;
    }

    public long insert(BookItem item) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(DBContract.BookItem.COLUMN_NAME_TITLE, item.getTitle());
        values.put(DBContract.BookItem.COLUMN_NAME_ISBN, item.getIsbn());
        values.put(DBContract.BookItem.COLUMN_NAME_AUTHOR, item.getAuthor());
        values.put(DBContract.BookItem.COLUMN_NAME_PUBLISHDATE, BookItem.FORMAT.format(item.getPublishdate()));
        values.put(DBContract.BookItem.COLUMN_NAME_SYNOPSIS, item.getSynopsis());
        values.put(DBContract.BookItem.COLUMN_NAME_CATEGORY, item.getCategory());
        values.put(DBContract.BookItem.COLUMN_NAME_IMAGE, item.getImage());
        values.put(DBContract.BookItem.COLUMN_NAME_NUMPAGES, item.getNumpages());

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(DBContract.BookItem.TABLE_NAME, null, values);

        return newRowId;
    }

    public void deleteAll() {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Define 'where' part of query.
        String selection = null;
        // Specify arguments in placeholder order.
        String[] selectionArgs = null;

        // Issue SQL statement.
        db.delete(DBContract.BookItem.TABLE_NAME, selection, selectionArgs);
    }


    public void close() {
        if (mDbHelper != null) mDbHelper.close();
    }


    public static BookItem getBookItemFromCursor(Cursor cursor) {

        long ID = cursor.getInt(cursor.getColumnIndex(DBContract.BookItem._ID));
        String title = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_TITLE));
        String isbn = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_ISBN));
        String author = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_AUTHOR));
        String publishDate = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_PUBLISHDATE));
        String synopsis = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_SYNOPSIS));
        String category = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_CATEGORY));
        String image = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_IMAGE));
        String numpages = cursor.getString(cursor.getColumnIndex(DBContract.BookItem.COLUMN_NAME_NUMPAGES));

        BookItem item = new BookItem(ID, title, isbn, author, publishDate, synopsis, category, image, numpages);

        return item;
    }
}
