package es.unex.giiis.asee.aseeproject.ViewModel;

import android.content.Context;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.asee.aseeproject.Repository.BookFavouriteRepository;
import es.unex.giiis.asee.aseeproject.Repository.BookRepository;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link Context}
 */
public class MainViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private static MainViewModelFactory instance;
    private final BookRepository mBookRepository;
    private final BookFavouriteRepository mBookFavouriteRepository;

    public MainViewModelFactory(Context context) {
        this.mBookRepository = BookRepository.getInstance(context);
        this.mBookFavouriteRepository = BookFavouriteRepository.getInstance(context);
    }

    public static MainViewModelFactory getInstance(Context context) {
        if (instance == null) {
            return new MainViewModelFactory(context);
        }
        else
            return instance;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new MainViewModel(mBookRepository, mBookFavouriteRepository);
    }


}
