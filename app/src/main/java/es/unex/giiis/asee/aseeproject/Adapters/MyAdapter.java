package es.unex.giiis.asee.aseeproject.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem;
import es.unex.giiis.asee.aseeproject.R;
import es.unex.giiis.asee.aseeproject.Models.BookItem;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<BookItem> bDataset;
    Context bContext;

    public interface OnListInteractionListener{
        void onListInteraction(BookItem bookItem);
    }

    public OnListInteractionListener bListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView bImageView;
        public View bView;

        public BookItem bBookItem;

        public MyViewHolder(View v) {
            super(v);
            bView=v;
            bImageView = v.findViewById(R.id.iv_book_item_list_poster);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<BookItem> myDataset, OnListInteractionListener listener, Context context) {
        bDataset = myDataset;
        bListener = listener;
        bContext = context;

    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_book, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.bBookItem = bDataset.get(position);

        Picasso.get().load(bDataset.get(position).getImage()).resize(500, 720).centerInside().into(holder.bImageView);
        holder.bImageView.setContentDescription(bDataset.get(position).getTitle());

        holder.bView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != bListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    bListener.onListInteraction(holder.bBookItem);
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return bDataset.size();
    }

    public void add(BookItem item) {

        bDataset.add(item);
        notifyDataSetChanged();

    }

    public void clear(){

        bDataset.clear();
        notifyDataSetChanged();

    }

    public void load(List<BookItem> items){

        bDataset.clear();
        bDataset = items;
        notifyDataSetChanged();

    }

    public void swap(List<BookItem> dataset){
        bDataset = dataset;
        notifyDataSetChanged();
    }

    public Context getContext() {
        return bContext;
    }
}
