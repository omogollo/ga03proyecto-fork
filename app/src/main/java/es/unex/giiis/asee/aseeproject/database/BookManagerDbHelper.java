package es.unex.giiis.asee.aseeproject.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


// Realiza las de gestiÃ³n de la bd

public class BookManagerDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "books.db";

    public static final String TEXT_TYPE = " TEXT";
    public static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_BOOKS =
            "CREATE TABLE "+ DBContract.BookItem.TABLE_NAME + " (" +
                    DBContract.BookItem._ID + " INTEGER PRIMARY KEY," +
                    DBContract.BookItem.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_ISBN + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_AUTHOR + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_PUBLISHDATE + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_SYNOPSIS + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_IMAGE + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_NUMPAGES + TEXT_TYPE +
                    " );";
    public static final String SQL_CREATE_BOOKSFAVOURITE =
            "CREATE TABLE "+ DBContract.BookFavourite.TABLE_NAME + " (" +
                    DBContract.BookFavourite._ID + " INTEGER PRIMARY KEY," +
                    DBContract.BookFavourite.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookFavourite.COLUMN_NAME_ISBN + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookFavourite.COLUMN_NAME_AUTHOR + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookFavourite.COLUMN_NAME_PUBLISHDATE + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookFavourite.COLUMN_NAME_SYNOPSIS + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookFavourite.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookFavourite.COLUMN_NAME_IMAGE + TEXT_TYPE + COMMA_SEP +
                    DBContract.BookItem.COLUMN_NAME_NUMPAGES + TEXT_TYPE +
                    " );";

    private static final String SQL_DELETE_BOOKS =
            "DROP TABLE IF EXISTS " + DBContract.BookItem.TABLE_NAME;

    private static final String SQL_DELETE_BOOKSFAVOURITE =
            "DROP TABLE IF EXISTS " + DBContract.BookFavourite.TABLE_NAME;

    public BookManagerDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i("Database_name", DATABASE_NAME);
        Log.i("Database_version", Integer.toString(DATABASE_VERSION));

    }

    /*
        Este metodo permite crear el esquema
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_BOOKS);
        db.execSQL(SQL_CREATE_BOOKSFAVOURITE);
    }

    /*
        Este metodo permite migrar el esquema de la base de datos
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_BOOKS);
        db.execSQL(SQL_DELETE_BOOKSFAVOURITE);
        onCreate(db);
    }
}
