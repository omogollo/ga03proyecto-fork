package es.unex.giiis.asee.aseeproject.Networking;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.unex.giiis.asee.aseeproject.Models.Bookshelf;
import es.unex.giiis.asee.aseeproject.Models.Item;
import retrofit2.Response;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.*;

public class RetrofitManagerUnitTest  {

    @Test
    public void getApiService() throws IOException {

        Bookshelf bookshelf = new Bookshelf();
        Item b1 = new Item();
        b1.setId("1");

        Item b2 = new Item();
        b2.setId("2");

        List<Item> books = new ArrayList<>();
        books.add(b1);
        books.add(b2);

        bookshelf.setItems(books);

        //Definir un objeto mapper a JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Crear un servidor Mock
        MockWebServer mockWebServer = new MockWebServer();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MockResponse mockResponse = new MockResponse();
        mockResponse.setResponseCode(200);
        mockResponse.setBody(objectMapper.writeValueAsString(bookshelf));
        mockWebServer.enqueue(mockResponse);

        //Linkear el el mock server con la api
        ApiService service = retrofit.create(ApiService.class);

        //Crear la llamada para obtener los libros
        Call<Bookshelf> call = service.listRepos("0",0,40,"0");
        Response<Bookshelf> response = call.execute();

        assertTrue(response!=null);
        assertTrue(response.isSuccessful());

        //Comprobar el contenido (body)
        Bookshelf booksResponse = response.body();
        assertFalse(booksResponse.getItems().isEmpty());
        assertTrue(booksResponse.getItems().size()==2);

        mockWebServer.shutdown();

    }
}