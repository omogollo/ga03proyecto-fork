package es.unex.giiis.asee.aseeproject.Models;

import org.junit.Test;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class BookFavouriteItemUnitTest {

    @Test
    public void getId() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 1);

        //when
        final long result = instance.getId();

        //then
        assertEquals("field no recuperado correctamente" , result, 1);
    }

    @Test
    public void setId() throws NoSuchFieldException, IllegalAccessException {
        long value = 1;
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden ", field.get(instance), value);

    }

    @Test
    public void getTitle() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        field.set(instance,"Deep Work");

        //when
        final String result = instance.getTitle();

        //then
        assertEquals("El campo no se devuelve correctamente", result,"Deep Work" );

    }

    @Test
    public void setTitle() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        String value = "Deep Work";
        instance.setTitle(value);
        final Field field = instance.getClass().getDeclaredField("title");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);

    }

    @Test
    public void getAuthor() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("author");
        field.setAccessible(true);
        field.set(instance,"Cal Newport");

        //when
        final String result = instance.getAuthor();

        //then
        assertEquals("El campo no se devuelve correctamente", result,"Cal Newport" );

    }

    @Test
    public void setAuthor() throws NoSuchFieldException, IllegalAccessException {
        String value = "Cal Newport";
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        instance.setAuthor(value);
        final Field field = instance.getClass().getDeclaredField("author");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);
    }


    @Test
    public void getIsbn() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("isbn");
        field.setAccessible(true);
        field.set(instance,"123456789");

        //when
        final String result = instance.getIsbn();

        //then
        assertEquals("El campo no se devuelve correctamente", result,"123456789");
    }

    @Test
    public void setIsbn() throws NoSuchFieldException, IllegalAccessException {
        String value = "123456789";
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        instance.setIsbn(value);
        final Field field = instance.getClass().getDeclaredField("isbn");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);


    }

    @Test
    public void getPublishdate() throws NoSuchFieldException, ParseException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("publishdate");
        field.setAccessible(true);
        field.set(instance,new SimpleDateFormat("yyyy").parse("2019"));

        //when
        final Date result = instance.getPublishdate();

        //then
        assertEquals("El campo no se devuelve correctamente", result, new SimpleDateFormat("yyyy").parse("2019"));
    }

    @Test
    public void setPublishdate() throws ParseException, NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        String date="2019";
        Date value = new SimpleDateFormat("yyyy").parse(date);
        instance.setPublishdate(value);
        final Field field = instance.getClass().getDeclaredField("publishdate");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);
    }

    @Test
    public void getSynopsis() throws IllegalAccessException, NoSuchFieldException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("synopsis");
        field.setAccessible(true);
        field.set(instance,"This book is best described as an attempt to formalize and explain my attraction to\n" +
                "depth over shallowness, and to detail the types of strategies that have helped me act on\n" +
                "this attraction. I’ve committed this thinking to words, in part, to help you follow my\n" +
                "lead in rebuilding your life around deep work—but this isn’t the whole story. My\n" +
                "other interest in distilling and clarifying these thoughts is to further develop my own\n" +
                "practice. My recognition of the deep work hypothesis has helped me thrive, but I’m\n" +
                "convinced that I haven’t yet reached my full value-producing potential. As you struggle\n" +
                "and ultimately triumph with the ideas and rules in the chapters ahead, you can be\n" +
                "assured that I’m following suit—ruthlessly culling the shallow and painstakingly\n" +
                "cultivating the intensity of my depth. (You’ll learn how I fare in this book’s\n" +
                "conclusion.)");

        //when
        final String result = instance.getSynopsis();

        //then
        assertEquals("El campo no se devuelve correctamente", result,"This book is best described as an attempt to formalize and explain my attraction to\n" +
                "depth over shallowness, and to detail the types of strategies that have helped me act on\n" +
                "this attraction. I’ve committed this thinking to words, in part, to help you follow my\n" +
                "lead in rebuilding your life around deep work—but this isn’t the whole story. My\n" +
                "other interest in distilling and clarifying these thoughts is to further develop my own\n" +
                "practice. My recognition of the deep work hypothesis has helped me thrive, but I’m\n" +
                "convinced that I haven’t yet reached my full value-producing potential. As you struggle\n" +
                "and ultimately triumph with the ideas and rules in the chapters ahead, you can be\n" +
                "assured that I’m following suit—ruthlessly culling the shallow and painstakingly\n" +
                "cultivating the intensity of my depth. (You’ll learn how I fare in this book’s\n" +
                "conclusion.)");

    }

    @Test
    public void setSynopsis() throws IllegalAccessException, NoSuchFieldException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        String value = "This book is best described as an attempt to formalize and explain my attraction to\n" +
                "depth over shallowness, and to detail the types of strategies that have helped me act on\n" +
                "this attraction. I’ve committed this thinking to words, in part, to help you follow my\n" +
                "lead in rebuilding your life around deep work—but this isn’t the whole story. My\n" +
                "other interest in distilling and clarifying these thoughts is to further develop my own\n" +
                "practice. My recognition of the deep work hypothesis has helped me thrive, but I’m\n" +
                "convinced that I haven’t yet reached my full value-producing potential. As you struggle\n" +
                "and ultimately triumph with the ideas and rules in the chapters ahead, you can be\n" +
                "assured that I’m following suit—ruthlessly culling the shallow and painstakingly\n" +
                "cultivating the intensity of my depth. (You’ll learn how I fare in this book’s\n" +
                "conclusion.)";
        instance.setIsbn(value);
        final Field field = instance.getClass().getDeclaredField("isbn");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);
    }

    @Test
    public void getCategory() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("category");
        field.setAccessible(true);
        field.set(instance,"Non Fiction");

        //when
        final String result = instance.getCategory();

        //then
        assertEquals("El campo no se devuelve correctamente", result,"Non Fiction");

    }

    @Test
    public void setCategory() throws IllegalAccessException, NoSuchFieldException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        String value = "Non Fiction";
        instance.setCategory(value);
        final Field field = instance.getClass().getDeclaredField("category");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);
    }

    @Test
    public void getImage() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("image");
        field.setAccessible(true);
        field.set(instance,"es/unex/giiis/asee/perrito.jpg");

        //when
        final String result = instance.getImage();

        //then
        assertEquals("El campo no se devuelve correctamente", result,"es/unex/giiis/asee/perrito.jpg");
    }

    @Test
    public void setImage() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        String value = "es/unex/giiis/asee/perrito.jpg";
        instance.setImage(value);
        final Field field = instance.getClass().getDeclaredField("image");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);
    }

    @Test
    public void getNumpages() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        final Field field = instance.getClass().getDeclaredField("numpages");
        field.setAccessible(true);
        field.set(instance,"320");

        //when
        final String result = instance.getNumpages();

        //then
        assertEquals("El campo no se devuelve correctamente", result,"320");
    }

    @Test
    public void setNumpages() throws NoSuchFieldException, IllegalAccessException {
        final es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem instance = new es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem();
        String value = "320";
        instance.setNumpages(value);
        final Field field = instance.getClass().getDeclaredField("numpages");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance),value);
    }
}