package es.unex.giiis.asee.aseeproject.RoomDB;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem;
import es.unex.giiis.asee.aseeproject.Models.BookItem;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

public class BookFavouriteItemDaoUnitTest {

    private BookFavouriteItemDao bookFavouriteItemDao;
    private BookDatabase db;

    // necessary to test the LiveData
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void createDb() throws Exception {
        //create a mock database, using our RoomDatabase implementation (AppDatabase.class)
        Context context = getInstrumentation().getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context, BookDatabase.class).allowMainThreadQueries().fallbackToDestructiveMigration().build();
        //take the dao from the created database
        bookFavouriteItemDao = db.bookFavouriteItemDao();
    }

    @After
    public void closeDb() throws Exception {
        //closing database
        db.close();
    }

    //Test correspondiente al caso de uso UC06
    @Test
    public void getAll() throws ParseException, InterruptedException {
        //creamos dos libros
        BookFavouriteItem b1 = createBook(998);
        BookFavouriteItem b2 = createBook(999);

        //insercion en la bd
        bookFavouriteItemDao.insert(b1);
        bookFavouriteItemDao.insert(b2);

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> liveBooks = bookFavouriteItemDao.getAll();
        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(liveBooks);
        //check that one of the elements is the first center
        assertTrue(books.get(0).getId() == 998 || books.get(0).getId() == 999);
        //check that one of the elements is the second center
        assertTrue(books.get(1).getId() == 998 || books.get(1).getId() == 999);
        //check that the same center does not return us twice
        assertFalse(books.get(0).getId() == books.get(1).getId());
    }

    @Test
    public void getByIsbn() throws InterruptedException, ParseException {
        //creamos dos libros
        BookFavouriteItem b1 = createBook(999);

        //insercion en la bd
        bookFavouriteItemDao.insert(b1);

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookFavouriteItemDao.getByIsbn("isbn");
        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);
        //check that one of the elements is the first center
        assertTrue(books.get(0).getIsbn().equals("isbn"));
    }

    //Test correspondiente al caso de uso UC03
    @Test
    public void deleteByIsbn() throws ParseException, InterruptedException {
        //creamos dos libros
        BookFavouriteItem b1 = createBook(999);

        //insercion en la bd
        bookFavouriteItemDao.insert(b1);

        bookFavouriteItemDao.deleteByIsbn("isbn");

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookFavouriteItemDao.getAll();
        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);
        //check that one of the elements is the first center
        assertTrue(books.size() == 0);


    }

    @Test
    public void insert() throws InterruptedException, ParseException {
        //creamos dos libros
        BookFavouriteItem b1 = createBook(998);
        BookFavouriteItem b2 = createBook(999);

        //insercion en la bd
        bookFavouriteItemDao.insert(b1);
        bookFavouriteItemDao.insert(b2);

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookFavouriteItemDao.getAll();

        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);

        assertFalse(books.size() != 2);
    }

    @Test
    public void deleteAll() throws InterruptedException, ParseException {
        //creamos dos libros
        BookFavouriteItem b1 = createBook(998);
        BookFavouriteItem b2 = createBook(999);

        //insercion en la bd
        bookFavouriteItemDao.insert(b1);
        bookFavouriteItemDao.insert(b2);

        //borrado en la bd
        bookFavouriteItemDao.deleteAll();

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookFavouriteItemDao.getAll();

        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);


        assertTrue(books.size() == 0);
    }

    //auxiliar method to create more easily centers to the tests
    public static BookFavouriteItem createBook(Integer id) throws ParseException {
        BookFavouriteItem b = new BookFavouriteItem();
        b.setId(id);
        b.setAuthor("author");
        b.setTitle("title");
        b.setCategory("category");
        b.setImage("image");
        b.setIsbn("isbn");
        b.setPublishdate(new SimpleDateFormat("yyyy", Locale.US).parse("1900"));
        b.setNumpages("10");
        b.setSynopsis("synopsis");

        return b;
    }
}